\chapter{Introduction}
\label{ch:intro}
%==================================================================================================
  \noindent Medicine has been notoriously slow to embrace the electronic medical record (EMR), but, spurred
  by tax incentives and the prospect of cost and outcomes accountability, the use of electronic medical records
  is finally catching on. There are a large number of EMR vendors, who offer systems that are either the
  traditional client server model (where the medical center hosts the system) or a product which can be delivered
  via Software as a Service (SaaS) architecture. Historically, the lack of extensive standards have allowed hospital
  components to be hard-coded into systems.  Any one company’s EMR system isn’t particularly compatible with the EMR
  system from another company; as medical practices often lack the ability to share basic information easily with one
  another. Nowadays, there’s widespread recognition that information exchange must improve – the challenge is how to get there!


  \section {Company Description}
    \noindent \textbf{Tulip\textsuperscript{\textregistered}} is the \emph{new player} in the Health Information Exchange \emph{arena}. We build customizable and fully standardized
    software for small to large medical groups -- hospitals and integrated healthcare organizations -- and their patients.
    We help with a unified suite of SaaS solutions build on top of a complex API proven to streamline administration, reduce costs
    and enhance patient safety.
    \par\vspace*{3mm}
    Our software enables multiple registration sessions for physicians, nurses, pharmacists and other
    authorized users and allows them to share data and streamline processes across an entire organization. An online dashboard
    displays up-to-date patient information in real time, complete with decision-support tools for physicians and other medical
    staff. Simple prompts allow swift and accurate ordering, documentation, and billing. All of these services can be deployed on
    any kind of device that supports an internet browser, from a simple PC to a tablet or a mobile phone.


    \pagebreak

    \section {Motivation and Goals}

    \begin{center}
      \emph{''The groundwork of all happiness is health.``}\\
      \hspace{55mm} James Leigh Hunt (1784 -- 1859)
    \end{center}


    \noindent \textbf{Tulip\textsuperscript{\textregistered}} embraces a culture of health. We build products that make us spend
    less time on getting the right health-care. So, no more worries for the best treatments, doctors, hospitals and medicines,
    hand-written prescriptions, annoying appointment calls, etc. Let us ‘orchestrate’ and bring that immediately to you.

    \par\vspace*{3mm}
    Our goals are driven by openness, R\&D (research and development) and standardization of a performant HIE (Health Information Exchange).
	\begin{itemize}
	    \item We make efforts to rapidly build the capacity for exchanging health information across the health care system both within and across countries.
	    \item We open our standardized data-model specifications to companies or organizations that want to adhere to a single and centered performant HIE core.
	    \item We externalize our API and by that a considerable amount of data to non-profit research organization that are in need of medical records for the
		  purpose of scientific experiments (Arrhythmia detection, brain activity patterns, etc.).
	    \item We try to keep up with all the best standards for software stack and data-models.
	    \item We aim to offer on demand customized applications (or pre-build templates otherwise) to our customers to fulfil their exact needs.
	    \item We provide full-support and online-courses, through our e-learning system, in which we explain how to properly use our products in order to the get best results.
	\end{itemize}

    \section{Management and Team Composition}

    \noindent Our company has two Headquarters (HQs), the main HQ is in Romania -- all technical teams, financial team and the management
    board -- and the second one in Amsterdam -- a part of our consultants, half-time developers, researchers, associate physicians, marketing, human resource and sales staff.

    \par\vspace*{3mm}

    Now, let us present our management board and teams’ composition:
    \begin{enumerate}
      \item \emph{ Management Board} (founders):
	  \begin{itemize}
	    \item CEO (chief executive officer);
	    \item CPO (chief product officer)\footnote{\label{CEO1} Responds to the CEO.}
	    \item CTO (chief technology officer)\footnoteref{CEO1}
	  \end{itemize}
      \item \emph{ Technical Team: }
	  \begin{itemize}
		\item 1 Lead Architect \footnote{Responds to the CPO \& CTO.}
		
		\item 2 Senior Architects (for front-end and back-end)\footnote{ Respond to the CTO and Lead Architect.}
		
		\item 3 Developers Teams:
		  \begin{itemize}
		      \item Front End Desktop Developers: GUI + middleware
			  \begin{itemize}
			  \item 1 team leader\footnote{\label{TL} Responds to the Lead and Senior Architects.}
			  \item 7 software engineers \footnote{\label{TM} Team members respond to their Team Leader and Senior Architects.}
			  \item 2 software engineers in test
			  \item 2 graphical designers
			  \item 1 experience designer
			  \end{itemize}
		      \item Front End Mobile Developers: GUI + middleware
			  \begin{itemize}
			    \item 1 team leader
			    \item 5 software engineers
			    \item 2 software engineers in test
			    \item 1 graphical designers
			    \item 1 graphical \& experience designer
			  \end{itemize}
		      \item Backend Developers
			  \begin{itemize}
			    \item 1 team leader
			    \item 14 software engineers
			    \item 4 software engineers in test
			  \end{itemize}
		      \end{itemize}
		
		\item 2 Q\&A Teams:
		    \begin{itemize}
			  \item Frontend Q\&A:
			       \begin{itemize}
				    \item 1 team leader \footnoteref{CPOCTO1}
				    \item 3 quality assurance engineers \footnote{ Team members respond to their Team Leader.}
				\end{itemize}
			  \item Backend Q\&A:
			       \begin{itemize}
				    \item 1 team leader
				    \item 5 quality assurance engineers
				\end{itemize}			
		    \end{itemize}
	    \item DevOp Team:
		    \begin{itemize}
		      \item 2 dev-ops \footnote{\label{CPOCTO1} Responds to the CPO \& CTO.}
		    \end{itemize}
		\item DBA Team:\footnoteref{CTO1}
		    \begin{itemize}
		      \item 2 database administrators
		    \end{itemize}
		\item  System Admin Team:\footnote{\label{CTO1} Responds to the CTO.}
		    \begin{itemize}
		      \item  2 system admins
		    \end{itemize}
		\item   IT assistant \& support team:\footnote{Respond to the system administrators.}
		    \begin{itemize}
		      \item 4 desktop assistants
		      \item 6 support operators (on field and call support)
		    \end{itemize}
	    \end{itemize}
      \pagebreak
      \item \emph{Scientists \& researchers} (all working together to gain value from our data):
	  \begin{itemize}
	   \item 1 lead scientist-researchers\footnote{Responds to the CTO.}
	   \item 3 data-scientists\footnote{Team members respond to the lead scientist-researcher.}
	   \item 2 data-analysts
	   \item 3 HIE researchers
	   \item 2 associate physicians
	  \end{itemize}

      \item  \emph{Flexy-team} (half-time or project based employed staff)\footnote{Respond to the leaders of the teams they are assigned to or to their full employed work-colleagues.}
	  \begin{itemize}
	   \item 5 -- 7 interns
	   \item 8 -- 10 half time employees or project based
	   \item 4 -- 5  consultants
	  \end{itemize}

      \item \emph{Human Resource Team}
	  \begin{itemize}
	   \item 2 HR specialists\footnote{\label{CEO2} Respond to the CEO.}
	  \end{itemize}

      \item \emph{Marketing \& Product Team}
	  \begin{itemize}
	   \item 1 marketing manager\footnoteref{CEO2}
	   \item 1 product analyst  \footnote{\label{CPO1} Responds to the CPO.}
	   \item 1 marketing analyst \footnote{\label{MM} Team members respond to the marketing manager.}
	   \item 4 marketing assistants \footnoteref{MM}
	  \end{itemize}


	\item \emph{Financial \& Sales Team}
	  \begin{itemize}
	   \item 1 account \& sales manager \footnoteref{CEO2}
	   \item 3 sales analysts \footnote{ Team members respond to the account \& sales manager.}
	   \item 1 account bookkeeper
	  \end{itemize}
    \end{enumerate}

    \section{Our Product}
    We present our product:\textbf{CareTracker\textsuperscript{\textregistered}} a complex application that tracks the medical process workflow, involving electronic
    medical records (EMR) \& a private health record (PHR), of a patient from his first visit to the doctor until his final recovery.


    Product Variants:
     \begin{enumerate}
      \item \textbf{CareTracker\textsuperscript{\textregistered} ProClinical }(Inpatient EMR) spans all hospital departments and specialties, giving providers
	  the tools they need to deliver safe, high-quality care. CareTracker’s role-based navigators simplify relevant information access for physicians, nurses,
	  therapists, dietitians and any other provider in the hospital setting.
      \item \textbf{CareTracker\textsuperscript{\textregistered} ProPhy} (Ambulatory EMR) is a physician-friendly system in use by providers representing more
	  than 20 specialties. It installs easily with our pre-built template system and configures to meet specific workflow requirements. Instead of starting
	  from scratch, our customers build on top of the content from successful other customers – including decision support, order sets, reports and documentation tools.
	  The result is a faster path to effective chronic care management and measurable quality gains.
      \item  \textbf{CareTracker\textsuperscript{\textregistered} CompletePatient (MyAssistant \& MyDashboard)}. Millions of patients access their records via
      \textbf{MyDashboard} -- literally the same dashboard used by their doctors. Patients can schedule appointments, get test results and print growth charts.
	  \textbf{Tulip\textsuperscript{\textregistered}}'s freestanding personal health record, \textbf{MyAssistant}, completes the circle, with an interoperable health
	  diary that can plug into MyDashboard -- or disconnect from it and inform care wherever the patient receives it.
	
	  \textbf{MyAssistant} is a PHR that is not connected to any facility's electronic medical record system. It stays with patients wherever they receive care and allows
	  them to organize their medical information in one place that is readily accessible. Patients can enter health data directly into MyAssistant,
	  pull in MyDashboard or upload standards-compliant Continuity of Care Documents from other facilities.
     \end{enumerate}

    The general workflow of the application is presented in Figure \ref{fig:flow}.
	
    \begin{figure}[htp]
    	\centering
    	\includegraphics[scale=0.4]{images/flow}
    	\caption{CareTracker: Application Workflow.}
    	\label{fig:flow}
    \end{figure}

    In Figure \ref{fig:bacfront} we describe our product’s architecture. The core part is represented by the API, the interface between the backend and the frontend. At the backend
    side (cloud based) we find a Database Cluster and a stack of services that intensively use the database through our data-infrastructure “pipe” (data platforms
    such as Hadoop, message queues for processing information, couple of custom engines, database drivers, etc). The interaction with our services is managed by the
    API (through interfaces and/or connectors, adapters).  At the other side, the frontend, we have 2 display applications, one for PCs browsers and one for mobiles or
    tablets. From the point of view of the application’s workflow process there is no difference between them, though the mobile application does not have the full list
    of features that are found on the browser side (such as: complex charts, archive listing, etc).  Both of the applications are built as SaaS (software as a service) and
    that gives us the advantage to easily extend our suite of features or add a new service-module. The communication (i.e packages’ transmission for the workflow
    synchronization) between the Web/Mobile Apps and the API is handled by network secured protocols.

     \begin{figure}[htp]
    	\centering
    	\includegraphics[scale=0.4]{images/bacfront}
    	\caption{CareTracker: Backend and Frontend Architecture.}
    	\label{fig:bacfront}
    \end{figure}


    \section{Software Development Strategy}

    As the \textbf{Tulip\textsuperscript{\textregistered}} software is a healthcare solution, the safety of patients is at stake. Therefore, a strong importance on system reliability, availability, acquaintance of the users with the functionalities and the interface, at all times, is emphasized throughout the development process. These non-functional
    requirements play a decisive role in the choice of the software development model approach.


    Given that the users, doctors or nurses employ the system in critical situations where timing is crucial, the releases should be well spaced in time and
    accompanied by training . The users should be aware of the changes and keep up with them. Nevertheless, frequent deployment of the releases in an isolated,
    non-critical, pre-production environment allows the development team to get faster feedback.


    As described in the Product section of this document, the functionality of the various product variants is vast, complex and closely integrated and should
    provide a high accessibility to well organized information.


    Maintaining a clear view of the appropriate patients data for a respective procedure is a challenge where only extensive usage of the software in real situations
    indicates the right approach. Additionally, the medical procedures involved are bound to evolve over time. We start from the assumption that we will make mistakes along the way. We do not consider this to be an exceptional event. 
    
    In consequence, the approach decided is to use an iterative development process, carried out in a pre-production environment described above. Each iteration cycle is planned for a 4 months duration, ideally accounting for 3 iterations a year. In the course of each cycle, an internal release is deployed for complete testing and collection of feedback. Releases in
    this controlled environment ensures that the software is thoroughly tested.
    
    At the end of 3 iterations, spanning one year, an external release is brought into production for all customers.


    The general structure of the iterative model is shown in the next diagram:
    \begin{figure}[htp]
    	\centering
    	\includegraphics[scale=0.5]{images/dev}
    	\caption{General structure of iterative model.}
    	\label{table:users}
    \end{figure}

    \pagebreak
    More specific, the approach taken can be broken down into the :

    \begin{enumerate}
     \item The major phases we distinguish are: Analysis, Design, Coding, Testing, Integration and Deployment.
     \item Each iteration will include some of the mentioned phases in different proportions.
     \item The project will progress along these lines:
	\begin{enumerate}
	 \item Carrying out several iterations, with emphasis on Analysis and Design. This step involves the use of prototyping to explore different design alternatives and ends when we are confident that we have arrived at successful results for these two phase, forming  the initial starting design.
	 \item The goal of the second stage is to collect feedback from users, identify problems from previous iterations and the new feature requests.
	 \item Based on the knowledge gained from the previous step, Re-Coding, Testing, Integration and Deployment are carried out in a controlled environment. The end of the iteration takes place in less than 4 months when the current version has reached a stable state, suitable for reliable usage in medical settings.
	 \item After the stable release has been made available for usage in the production environment, return to step 3b) for creating another iteration for the general users based on the feedback collected.
	\end{enumerate}
    \end{enumerate}

      \textbf{Programming languages used in development:} Java \& Python\\
      \textbf{General/universal quality standards:} ISO 9000/3166/639\\
      \textbf{Medical software standards:} HL7, CEN set\\
      \textbf{Business workflow process standards:} XPDL





    \section{Market Insight}

    Our users are doctors, nurses, and pharmacists (if the pharmacy is part of the hospital), but also patients of the hospitals that have acquired our software.
    \textbf{CareTracker\textsuperscript{\textregistered}} is available in 5 countries (Romania, Netherlands, Belgium, Hungary and Bulgaria), counting a high number
    of users, presented in the table below.

     \begin{figure}[htp]
    	\centering
    	\includegraphics[scale=0.5]{images/users}
    	\caption{CareTracker: Intended users.}
    	\label{table:users}
    \end{figure}

    \textbf{CareTracker\textsuperscript{\textregistered} ProClinical} and \textbf{CareTracker\textsuperscript{\textregistered} ProPhy }are intended for medical personnel
    only and patients do not have access to it, while  \textbf{CareTracker\textsuperscript{\textregistered} CompletePatient} is specially designed for patients.

    \subsection{Way of Distribution}
    Clients can buy in our online store a key which gives them access to our web applications. This key gives them a limited number of patients which can be managed
    using this software. Clients can be either hospitals or private medical offices. Once they have bought the access key, they can start inserting patients’ personal data.
    When required data is filled, they receive usernames and passwords for the patients. As soon as the authentication details are given to the patients, they can start using
    \textbf{CareTracker\textsuperscript{\textregistered} CompletePatient}. For technical support clients can contact us by phone or email and we provide remote
    support (using VNC\textsuperscript{\textregistered}, TeamViewer\textsuperscript{\textregistered}).

     \par\vspace*{3mm}

    At any time our free mobile application can be downloaded from Google Play (Android application), but personal data can be accessed only after the hospital
    provided the patient their username and password.

     \par\vspace*{3mm}

    For learning how to use the software or training their medical personnel we provide an e-learning platform which can be accessed at any time.

     \par\vspace*{3mm}

    We promote our online store by using the feature provided by Google, AdWords.







