\chapter{Branching \& merging}
\label{ch:merging}

A branching and merging strategy implies a tradeoff between risk and productivity. Towards, we want to minimize the risk taken while increasing productivity. This translates into sacrificing the safeness of working in isolation for the increased productivity of working in a team. Sometimes, the productiveness comes with a downside - in future some effort will be required to merge the software written by different people.

Using branches is a solution to have an increased productivity, as different people may work in parallel. However, this requires effort when merging the branches back into a whole. Figure~\ref{fig:branchingTask} shows a branching \& merging example.

\section{Branching Policy}

We decided that a branch is aligned with work breakdown structures\footnote{WBS}, meaning that branching is oriented on solving tasks by dividing the system in subcomponents. We isolate a task in a branch to avoid overlapping tasks and productivity loss. These branches are short-term and must be merged as soon as the task is finished. 

\begin{figure}[htp]
	\centering
    	\includegraphics[scale=0.6]{images/BranchingByTask}
    	\caption{Branching by task}
	\label{fig:branchingTask}
\end{figure}

\begin{itemize}
    \item The owner of a branch is usually the team leader or the lead architect (for releases), but for feature branches or bug fixes the owner can be one of the developers working on the branch.
        The lead architect and team leaders decide who is allowed to branch and when. For example, the lead architect may decide that the Desktop team should make a new release, while the team leader may decide that developer A works on adding a feature.

    \item Architects are one iteration ahead, so they know exactly what is next. Usually, team branches are created at the beginning of a new iteration. Team leaders may decide to make a development branch, followed by test or report branches.

    \item Branching for libraries is allowed and decided by the lead architect. This may happen when a new stable version of a library, which provides features that we need, is out. This kind of branching should be rare, and must include integration. Previous versions (which use the old library) should work as before, and the following versions should take advantage of the functionalities of the new library.

    \item When work on a branch is finished, it goes through a process of QA and Integration. If these fail, new subbranches for bug fixes and testing are made. If they succeed, the owner of the branch merges the changes in the main codeline (or parent branch). If, while merging, there are conflicts, the old versions get priority when dealing with libraries. If the new version of the library is necessary, but does not ensure backward compatibility, both versions are maintained and used by our system (by different releases). If there are conflicts not related to libraries, branch owners should resolve the conflict together by analyzing the requirements and the code.
\end{itemize}

\section{Branch Strategy and Plan}

We will use multiple branching strategies, depending on the circumstances.

\subsection{Branch per Iteration}
This strategy is used for separating individual iterations from each other. We create a branch at the beginning of the iteration and do the entire work on that branch. The owner of this branch is the lead architect, which makes sure the technical standards and the design he dictated are respected. He takes care of the branch plan, and updates it at the end of each iteration. Besides him, all developers involved in the current iteration work on this branch.

The branch per iteration ends when an iteration ends, resulting in a product release. It also consists of QA\footnote{Quality Assurance} and Integration Tests, which makes it more stable than any other branches. It is created for maintaining the trunk stable (no work is done directly on trunk, but only on branches).

\subsection{Branch per Team}
Each of our teams can do work in their own branch. This branch is maintained by multiple developers who may, in turn, have their own sub-branches during development efforts. The owner of the branch is always the team leader, while every other developer which is part of that team may work on the branch as well.

This branch is important because development on different platforms has a different structure (for example, \textit{Android} development is distinct from a \textit{Desktop Application} development). We may have tasks for the \textbf{Front End Desktop Developers Team}, but not for the \textbf{Front End Mobile Developers Team}. This branch will exist until all tasks set for the current iteration for a certain team are finished and tested. When this happens, the branch is merged into its \textbf{Iteration Branch} (without necessarily resulting in a product).
The branch plan is made by the owner of the branch, usually the team leader, and is constantly maintained accurate by him. Quality is assured by having a branch test-state (regression tests, smoke tests) where before merging everything is tested.

\subsection{Branch per Feature}
 A branch is created for a feature to be developed from its start to its finish. Once the feature is ready it must be merged back into another branch for continuous work. In this way, our teams can work in parallel on different features.

 The owner of this branch is elected by the team leader and is one of the developers that will work on the feature. He takes care and updates the branch when a feature is finished. This branch results in a merge with a team-branch when the implementation of the feature is done. All features should be atomically as possible and the development process should abide by the Open Close Principle\footnote{"Software entities (classes, modules, functions, etc.) should be open for extension, but closed for modification"}.

 There is also a testing phase in this branch, as shown in figure~\ref{fig:strategy}.

 \begin{figure}[htp]
	\centering
    	\includegraphics[scale=0.4]{images/Strategies}
    	\caption{All strategies used}
	\label{fig:strategy}
\end{figure}

Besides the branch strategies described above, there can also exist subbranches. For example, one team can have a main branch for a certain feature, but there might be a subbranch to fix a bug. The subbranching is decided by the team leader and he also makes sure that developers work on the assigned branch.

 \subsection{Merging}
 Merging means closing a branch by putting all the changes into the mainline (which may be another branch). For all strategies, merging is done with automated tools by the developers. It may fail because the exact same portion of code was modified by another developer, so merging must be made manually. To make merging simpler, a developer should first merge the main branch in his private branch (the main branch might have changed in the meanwhile).

 To avoid as much as possible conflicts, a developer can set a \textit{watch} on a file, and others will be notified if they want to modify the same file.
 
 At the end of an iteration, each branch is merged in the iteration branch, and the iteration branch is merged in the mainline, as shown in figure~\ref{fig:strategy}.

 \subsection{Branch roles}
 There are 5 main roles that need to be considered for branches in planning toward the goal of a single release:

\begin{itemize}
    \item mainline
    \item development
    \item maintenance
    \item accumulation
    \item packaging
\end{itemize}

\subsubsection{Mainline}
The mainline is an important role in the proper management of a development effort. The purpose of a mainline is that of a central codeline to act as the basis for subbranches and their resultant merges.
The life span of the mainline is the life span of the code base.

\subsubsection{Development}
 Development is the activity that produces feature and function enhancements. In our case, for each iteration we will have several branches with this role. It is a higher risk activity than the simple fix, as a new functionality is created. This branch type is finished when the development project achieves its goal.

\subsubsection{Maintenance}
 Maintenance usually designates bug fixing activities. Most bug fixes have a lower risk than development projects. We can perform bug fixes on the mainline also. The scope of maintenance is smaller, therefor making the merge policy a lot simpler.


\subsubsection{Accumulation}
The branch satisfying the accumulation role acts as the focus for merging the final results of various subbranches. When the changes that have not been merged are substantial, it is recommended to use two steps for integration. The first step is to merge the branch in another branch, followed by merging the code in the mainline. This branch will tend to have a short life span, spanning only the time necessary to integrate the projects and fix any conflicts.

\subsubsection{Packaging}
Using a separate branch to insulate the release effort from the ongoing development and maintenance, and vice versa, is recommended. We will use a separate packaging branch per component (core, desktop and mobile).

\section{Naming}

We established a naming convention that indicates the work the branch contains. For example, branch \textbf{it1-mob-2.1.9} is the branch that contains all the mobile application code at the end of the first iteration, which will be part of a stable build with the same major \& minor version number. Some examples are found in table~\ref{table:branchName}.

\begin{center}
\begin{table}[htb]
	\centering	
	\caption{Naming convention rules for branches}
    \begin{tabular}{|l|p{5cm}|p{4cm}|}
    \hline
    \textbf{Branch type}& \textbf{Naming Convention Rule} & \textbf{Example} \\
    \hline
    Mainline Branch & [product]-main & caretracker-main \\
    \hline
    Iteration Branch - main & it[iterNo]-main & it3-main \\
    \hline
    Team Branch - main & it[iterNo]-[team][teamNo]-main & it3-desk1-main \\
    \hline
    Feature Branch & it[iterNo]-[team][teamNo]-[version]-featname & it3-desk1-2.3-loginWithGoogle \\
    \hline
    Patch Branch & it[iterNo]-[team][teamNo]-[version]-featname-patchtype & it3-desk1-2.3.6-loginWithGoogle-bugfix \\
    \hline
    Component Release Branch & it[iterNo]-[component]-[version] & it3-web-2.3.6 \\
    \hline
    \end{tabular}
  	\label{table:branchName}
\end{table}
\end{center}


For the version number, we have decided to use three levels of version numbers connected with periods (e.g. 2.2.1). The first number is associated with \textbf{Major} version, which has significant feature enhancement (incremented at each annual release). The second number is the \textbf{Minor} version, which has less feature enhancements and a number of bug fixes. The Minor is incremented at each iteration. The last number refers to a patch level and always contains bug fixes (it is incremented at each bug fix).

When merging all the components' release branches into the iteration branch, we put a tag with a version number equal to the last \textbf{Major.Minor.Patch} on it. Then, after we have achieved a stable build we merge the iteration branch into it's parent branch (product's mainline branch) and tag that point with a version number equal to the current \textbf{Major.Minor}. This tag basically means that an iteration was successful and it's last stable builds can reside in a product's release (see chapter~\ref{ch:relmanage}).