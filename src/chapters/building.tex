\chapter{Building}
\label{ch:building}


\section{Build description}

The build process involves in general the following CI:
\begin{itemize}
  \item source code located in: \\
      \textit{ /product-name/main/component-name/src}
  \item library dependencies list located in: \\
      \textit{ /product-name/main/component-name/lib}
  \item shared libraries located in: \\
      \textit{ /product-name/lib}
  \item build plans  (CI alias: comp-doc ) located in:\\
      \textit{ /product-name/main/component-name/doc}
  \item build make files(CI alias: comp-script) and unit tests located in: \\
	\textit{  /product-name/main/component-name/script}
\end{itemize}

\subsection{Build Automation}
The build, deploy and test process is carried out by the Bamboo service (provided that we upload scripts for building our product), see chapter~\ref{ch:identification}, which integrates with the source code repository, Bitbucket, and other Atlassian tools used in the development process as Confluence or Jira.

\subsection{Building Tool for Java}
Bamboo allows automatic or manual regularly (nightly build) triggering of the build tool when changes are committed. Given a Java implementation of the product the Apache Ant tool is used for managing the build process.

The history of builds, including versions, result, initiator is maintained by \textbf{Bamboo} ``Build Result Summary'' feature and exported along with the scripts \& executables to the file system located in:
\\
    \begin{itemize}
     \item \textit{/product-name/script}
     \item \textit{/product-name/build} accordingly to the type of release:
	  \begin{itemize}
	   \item \textit{/last-stable}
	   \item \textit{/nightly}
	   \item \textit{/release}
	  \end{itemize}
    \end{itemize}
	
\section{Build process}

\subsection{Build Strategy}
The choice of build control framework has been done to allow Continuous Integration besides regular and manually triggered builds. The motivation behind using Continuous Integration is that Tulip source code is expected to compile in a matter of minutes and therefore frequent builds are convenient for detecting coding errors without introducing compiling bottlenecks.

The build process is governed by 3 types of specific build rules.

\subsubsection{Nightly Build}
Overnight builds are scheduled through the Bamboo framework and run automatically for every component. A nightly build assumes autonomous merging of the ongoing branches into a temporary common branch used for running the nightly build. The status of the merging and resulted build is recorded and reported by the framework.

The nightly build rules, including the configuration related to build source code (branches and versions) are implemented based on the build plan (CI alias: comp-doc) by the architects with support from the administrators.

\subsubsection{Stable Build}
At the end of each branch merging, the modified components are built and smoke and integration testing is carried out. The integration tests are represented by the \textit{integration-test-base/version-number/} CI.

If these builds pass the testing phase, the builds are labeled as stable and a new baseline containing the stable build version is created and brought under configuration control.
The build depends on the unit tests CI's: \textit{unit-test-core-api-base/version-number/|}, \textit{unit-test-mob-app-base/version-number/}, \textit{unit-test-web-app-base/version-number/}.

The stable builds are scheduled by our architects in collaboration with the rest of team leaders.

\subsubsection{Release Build}
The release build is scheduled and managed by the product owner, the CPO, and the DevOp Team.

After 3 iterations of development, testing in the controlled environment described in Section ~\ref{ch:intro}, the resulted stable builds are assessed by QA through validation and testing. Once approved by QA the stable builds are labeled as release builds and become the next version of Release baseline (release-base/version-number/), with each individual build corresponding to one of the sub-baselines: \textit{core-api}, \textit{mob-app}, \textit{web-app}.

This build will be published and made available to the customer.

The build rules are further more documented in the \textit{comp-doc CI}, see figure~\ref{fig:ci}, and specified in the Bamboo build service.

\subsection{Variations in build}
\begin{itemize}

\item	Configuration selection:

In addition to the three type of builds, the build system should allow for selection of the specific versions of the sub-components and dependencies per build and record this configuration in the table below, ~\ref{table:buildtable}.

\end{itemize}

	\begin{table}[htb]
        \centering
        \caption{Table for builds' version recording - is filled automatically when a build is successful.}
	    \begin{tabular}{|l|*{4}{l|}}
        \hline
      	\textbf{Item} & \textbf{Component Version No.} & \textbf{Nightly} & \textbf{Stable} \\
        \hline
      	core-api & 1.3.12 & 1.3.b12.4 & 1.b3.2 \\
      	\hline
      	mob-app & 1.3.7 & 1.3.b7.2 & 1.b3.1 \\
      	\hline
      	web-app & 1.3.21 & 1.3.b21.5 & 1.b3.3 \\
      	\hline
    	\end{tabular}
   		\label{table:buildtable}
   	\end{table}

\subsection{Naming}
The naming of the builds follows the pattern:\\
\textbf{<component_name>-<build_type_version_number>}

The build type is specified accordingly to the 3 types described in the build process. \\
The \emph{version number} follows the naming conventions from the table~\ref{table:buildnameconv}. \\
The \emph{major number} and \emph{minor number} correspond to the requirements baseline version number (name: \textit{req-base}) that the build is subjected to. \\
Additionally the nightly build names contain the \emph{max_patch} identifier referring to the latest patch incorporated in the respective branch. \\
The \emph{build_number} restarts from 1 at every new iteration and is incremented at each build. \\

\begin{center}
\begin{table}[htb]
  \centering
  \caption{Naming convention rules for builds.}
  \begin{tabular}{|l|*{2}{l|}}
    \hline
    \textbf{Build Type} & \textbf{Naming Convention Rule} & \textbf{Example} \\
    \hline
    Nightly & [comp]-[major].[minor].b[max_patch].[build_number] & web-app-1.1.b20.4 \\
    \hline
    Stable &  [comp]-[major].b[minor].[build_number] &  web-app-1.b1.21 \\
    \hline
    Release & \textit{see chapter~\ref{ch:relmanage}} & -- \\
  	\hline
  \end{tabular}
  \label{table:buildnameconv}
\end{table}
\end{center}
