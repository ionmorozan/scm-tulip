\chapter{Status Accounting}
\label{ch:accounting}

Software Configuration Status Accounting (CSA) provides reporting mechanisms to ensure the integrity of the 
\textbf{CareTracker\textsuperscript{\textregistered}} system's configuration at any time.
With proper configuration management status accounting, the current and previous configurations 
of the \textbf{CareTracker\textsuperscript{\textregistered}} products can be reported to the change 
control board (CCB) and managed appropriately.

The main mechanisms provided by our CSA process are:
\begin{itemize}
 \item identification of the current approved configuration documentation and indentification number associated with each CI.
 \item record and report the status of proposed changes from the initial to the final approval.
 \item record and report the status of all major requests deviation which affect the configuration of a CI.
 \item record and report implementation status of authorized changes.
 \item supports inquiries concerning future planning of design changes, investigations of design problems.
 \item access to complete configuration information on the \textbf{CareTracker\textsuperscript{\textregistered}}
       products and any individual product unit, or group of product units.
 \item access to accurate identification of the configuration of each delivered product unit.
\end{itemize}

Diagram~\ref{fig:cr} describes the process of a change request.

\begin{figure}[htp]
	\centering
    	\includegraphics[scale=0.5]{images/CR.pdf}
    	\caption{Change Request Process.}
	\label{fig:cr}
\end{figure}

All changes that happen to each CI and all its associated activities performed on them are recorded, so individual changes 
can then be tracked through each version as they occur. The tool used in order to accomplish data collection and reporting tasks is
Confluence~\footnote{http://www.atlassian.com/software/confluence/overview/team-collaboration-software} which stores all these
information in a MySQL relational database, so retrieval of the information is fast and efficient.


\section{Stakeholder information needs}
The aim of status accounting is to keep managers, users, developers, and testers informed about the configuration 
stages and their evolution.

Team management uses the information provided by the status accounting in order to identify problems, source of the
problems and take action before they become critical. Developers check the SA to see how the software evolved during
time and what each unit of the software could offer. Then, developers could propose changes and features in order to
improve the stability and quality of our software. The status accounting is invaluable during the testing phase. In order 
to understand the cause of a failed test one needs to know about the history of the CI. Identifying the changes that were 
to the program since a predefined date  could resolve the problem faster.

Our team is using the information provided by the status accounting to determine the performance characteristics of our products, such
as number of change requests, approval rate, number of problem reports, average implementation time and cost of implementing a change.

All encountered changes during different phases of the software development cycle of our products are recored into a database. Knowing the changes that have been made lead to accurately up to date CIs. The database is the primary reference point for anyone interested in the progress of \textbf{CareTracker\textsuperscript{\textregistered}} products and captures enough details to create appropriate reports for each team.

A simple SA report contains:
\begin{itemize}
 \item CI name and identification number;
 \item Design start date;
 \item Design approval date and revision number;
 \item Coding start/finish dates;
 \item Testing start/finish dates;
 \item Build start/finish dates and revision number;
 \item System merge date;
 \item Change request/incorporation date;
\end{itemize}

Our system provides two types of reports:
  \begin{enumerate}
   \item \textbf{Routine reports} including: 
      \begin{itemize}
      \item change log;
      \item progress report;
      \item status report;
      \item transaction log;
      \end{itemize}
   \item \textbf{Ad hoc reports}, when a user requests particular information that is not included in the routine reports. Some examples are as follows:
      \begin{itemize}
       \item list of all CRs that have been approved;
       \item list of how many people are working on a CR;
       \item number of CRs that are pending;
      \end{itemize}
  \end{enumerate}
  
 The objective of the Configuration Status Accounting Report (CSAR) is to provide: visibility into the current status of the Configuration Items that are currently being under development, documentation and identification numbers relating to those CIs, and changes to the items and their configuration documentation.
 
\section{Reports}

The reports are used by various organizational and project elements, such as the development team, the maintenance team, project management and QA team so
in the end all the project stakeholders are aware of the latest version of the project.

The routine reports can be described as follows:
\begin{description}
 \item [ Change Log ] -- contains information about all Change Requests issued. This type of report is generated monthly and contains details such as
			  CR number, status, name of the initiator, affected units of our products and description of the change.
 
 \item [ Progress Report ] -- provide information about the progress of the project, briefly describing the work performed during an iteration. 
			    It is primarly used by the management team. It is issued once every 4 months or at the end of an iteration.
 
 \item [ CI Status Report ] -- summarize the status of all CIs in the system. It contains the name and version number of CIs and details of dependent items.
			      This report is generated monthly.
 
 \item [ Transaction Log ] -- contains all the changes that happens to items during a specific period. It includes details such as transaction number, date, affected
			    units, description, activity(CR, CCB approval, problem report) and remarks. This report is generated on demand with Confluence.
\end{description}

All reports are being stored in a private cloud to prevent unauthorized person to access the data and avoid inconsistencies in case of a hardware failure (hard disk crashes).

  \subsection{Automation of Status Accounting}
  
  In order to ease the process of Status Accounting we are using Confluence, a tool from the Atlassian suite, which provides real-time dynamic data in front
  of users. Also, it offers collaborative features such as automatic versioning, granular enterprise security, roll back and update notifications. Confluence stores
  this information in a MySQL database, so retrieval of the information is fast and efficient, the data being up to date as the events are recorded and is captured as the
  activities occur.

  Our tool offers the possibility to generate routine and ad-hoc reports. With basic SQL knowledge the user can query the system for any information they need
  and get the answers immediately in the format they want. 
  
  \newpage
  
  Confluence provides a variety of features such as :
    \begin{itemize}
     \item retrieving data into tables
     \item visualizing data with charts and formatting
     \item creating dynamic content
    \end{itemize}
   
   and it is able to automatically create the following reports:
   \begin{description}
    \item [Change/Problem Tracking Reports] -- tracks details such as who made the changes, when, who initiated it and CR status.
    \item [Difference Reporting] -- keeps track of the difference between versions and releases.
    \item [Ad hoc Queries] -- allows our stakeholders to get customized information, when they want and in the format they want.
   \end{description}

   Even though our SCM tools automate the status accounting process making it easier and accurate, not all the information is gathered automatically.
   For example our system is not fully capable of generating a global report which covers all data from all the products, because the development of
   different products takes place at different times and may contain different objectives. This often involves a manual gathering of data from 
   the various products and a manual synchronization of the data to a global view.
    
    In future we want to standardize this practice and to gain more visibility of data across the organization either considering or 
    actually implementing an Enterprise Data Warehouse (EDW) to perform global reporting. This EDW will gather data from different products and
    provide reporting at both enterprise and operational levels.
    
    The status accounting function plays an important role in our efficient product management and control. We proved that our SCM tools automate the status 
    accounting function and help users with information that is accurate, timely, and relevant.


  
