\chapter*{Software Configuration Management}

\chapter{Software Configuration Identification}
\label{ch:identification}

In this chapter we are going to identify the configuration items within our product, \textbf{CareTracker\textsuperscript{\textregistered}}. Though we have 3 product variants, the \textbf{CareTracker\textsuperscript{\textregistered} ProClinical}, \textbf{CareTracker\textsuperscript{\textregistered} ProPhy} and \textbf{CareTracker\textsuperscript{\textregistered} CompletePatient} are different only from the market's point of view -- they aim different client-category targets. From a technical point of view (as a software application), these variants are actually one and the same product, but with multiple profiles and session layers. Notably, you login into the CareTracker application and further on you are redirected to your own session, which depends on what type of user you are. Thus, the following chapters and sections focus on CareTracker\textsuperscript{\textregistered} and its main components' -- CoreAPI (backend), WebApp (desktop frontend) and MobileApp (mobile frontend) -- configuration management process (identification, branching, merging, baselines, build phase and so on). These are our software developers' main concerns that need to be clarified and described in the following paragraphs.

\section{File System}

Everything our company does is stored -- a part of the storage, \textit{home} and \textit{utilities} directories, into our local storage servers (internal backups) and the rest of it, \textit{product, system, datastore} into a private cloud-storage system (automatically fail-over, replication and backups). This includes reports, test cases, test reports and so on. Therefore, the file system contains more than just the application itself. Our efficient and clean structure layout allows employees to instantly find what they are looking for. We have illustrated the first 2 levels of our file-system in figure~\ref{fig:fs}.

\begin{figure}[htp]
	\centering
    	\includegraphics[scale=0.7]{images/fs}
    	\caption{First 2 levels of our file-system}
	\label{fig:fs}
\end{figure}

\pagebreak

We can further extend figure~\ref{fig:fs} with the following hierarchy of folders:

\begin{description} \itemsep0pt \parskip0pt \parsep0pt
	\item[/tulip] - root point
	\begin{description} \itemsep0pt \parskip0pt \parsep0pt
		\item[/home] - private storage space for each employee [besides their local PC space]
		\begin{description} \itemsep0pt \parskip0pt \parsep0pt
			\item[/mng-board] - management board private space
			\begin{description} \itemsep0pt \parskip0pt \parsep0pt
				\item[\ldots] \hfill
			\end{description}			
			\item[/team] - teams' private space
			\begin{description} \itemsep0pt \parskip0pt \parsep0pt
				\item[\ldots] \hfill
			\end{description}
		\end{description}
		\item[/utility] - internal-utility items
		\begin{description} \itemsep0pt \parskip0pt \parsep0pt
			\item[/employee-training] - tutorials and books
			\item[/software-tools] - tools such as IDEs, text-editors, compilers, \ldots
			\item[/operating-sys] - Windows/Unix install images
			\item[/sandbox-env] - deployment scripts for specific test environments [remote/local testing environment deploys]
		\end{description}
		\item[/product] - product related items [developments \& builds]
		\item[/system] - system related items [product SaaS deployment scripts, monitoring, maintenance scripts/daemons, \ldots]
		\item[/datastore] - database related items [migration, backup scripts/daemons, \ldots]
	\end{description}
\end{description}

We are now going to detail the main entry point of our file-system, the \textcolor{red}{\textbf{/product}}:

\begin{description} \itemsep0pt \parskip0pt \parsep0pt
	\item[\textcolor{red}{/product}] \hfill
	\begin{description} \itemsep0pt \parskip0pt \parsep0pt
		\item[/CareTracker] \hfill
		\begin{description} \itemsep0pt \parskip0pt \parsep0pt
			\item[/lib] - all internal/external libraries
			\item[/main] - components \textbf{(1-3)}
			\begin{description} \itemsep0pt \parskip0pt \parsep0pt
				\item[/web-app (1)] - the web desktop front end
				\begin{description} \itemsep0pt \parskip0pt \parsep0pt
					\item[/lib] - a dictionary-file of component's library dependencies \& scripts to get them from top \textit{/lib} to employee's local \textit{/lib} [by this we omit duplication within components' \textit{lib} directories]
					\item[/src] - source codes [component's modules, interfaces and so on]
					\item[/test] - unit and regression test-suites
					\item[/report] - component test reports
					\item[/profile] - profiling analysis
					\item[/doc] - component specific technical documentation: code, algorithms, interface, tests and design documentation
					\item[/resource] - resource items [i.e. images]
					\item[/script] - scripts for component local-testing \& private-building -- for pre-commit continuous testing
				\end{description}
				\item[/mobile-app (2)] - the mobile lightweight front end
				\begin{description} \itemsep0pt \parskip0pt \parsep0pt
					\item[\ldots] same hierarchy as the web-app
				\end{description}
				\item[/core-api (3)]	- the core API back end
				\begin{description} \itemsep0pt \parskip0pt \parsep0pt
					\item[\ldots] same hierarchy as the web-app
				\end{description}
			\end{description}
			\item[/test] - integration, system and accepting test-suites
			\item[/doc] - product specific documentation: requirements, specifications, architecture, manuals, full test-specs
			\item[/script] - scripts for product testing \& building  [i.e build in release mode]
			\item[/build] - product build versions
			\begin{description} \itemsep0pt \parskip0pt \parsep0pt
				\item[/last-stable] - last stable build
				\item[/nightly] - overnight build
				\item[/release] - release build
			\end{description}
		\end{description}
	\end{description}
\end{description}

\section{Configuration Items Within The System}
\label{sec:configurationItems}

Our hierarchy of CIs branches out from \textbf{/product, /system, /datastore, /sandbox-env} entry points within our system. However, to keep it simple we are going to discuss only about the configuration items within our \textbf{/product} -- briefly described in figures~\ref{fig:ci},~\ref{fig:cimod}. The last three items are managed by small teams (2-3 members) and are less complex (only scripts and small daemon programs) than our main product, so they do not show great interest for the purpose of the present software configuration plan.

\begin{figure}[htp]
	\centering
    	\includegraphics[scale=0.65]{images/ci}
    	\caption{Hierarchy of CI's [1] - repository template}
	\label{fig:ci}
\end{figure}

\begin{figure}[htp]
	\centering
    	\includegraphics[scale=0.65]{images/ci-mod}
    	\caption{Hierarchy of CI's [2] - top modules}
	\label{fig:cimod}
\end{figure}

As you can see from the figures~\ref{fig:ci},~\ref{fig:cimod} our CIs tree is very modular (allows multiple teams to work at the same time on multiple components and test-suites) and testable (we have component and product based tests stored/versioned separately such that testers and QAs do not overlap during their work). However, the inside structure of \textit{/src and /test} directories is established by our team-leaders and senior-architects. They decide how the application-modules' design looks like (what is inserted/deleted). One thing for sure is that it avoids as much as possible further development conflicts. Developers have separate tasks on well defined modules to work on, i.e. core-api:service.engine.search:task=DefineJsonIndexDocumentModel. For example, if a new module has to be created, our software engineers will begin to code and use our versioning-system to save and share their sources. Obviously, everything starts beforehand with a thoroughly process of writing documents (module \& test
specs, requirements and so on). Meanwhile, a corresponding test-suite will is developed by our testers and QAs in their own fashion (each one from another perspective -- component/product based). In addition, to avoid any disruptive conflicts over the test developments, only the binaries and scripts are shared outside (i.e to third-parties), not the code. We aim to keep our components and test-suites as 'black-boxes'. This policy applies within all of our teams -- you need to be part of a team to have access to specific files, otherwise public access is only ensured for the deliverables mentioned in the product's documentation. A client distribution package contains a unique-generated-key for accessing and downloading the purchased product-pack (i.e CareTracker\textsuperscript{\textregistered} ProPhy for 150 patients) from our online market repository, afferent documentation and free login credentials for the e-learning platform.

\subsubsection*{Owners and Responsibilities:}
\begin{description} \itemsep0pt \parskip0pt \parsep0pt
	\item[/CareTracker] - owner: CPO
	\begin{description} \itemsep0pt \parskip0pt \parsep0pt
		\item[/lib] - architects
		\item[/main] \hfill
		\begin{description} \itemsep0pt \parskip0pt \parsep0pt
			\item[/web-app, mob-app, core-api] - owner: front/back-end senior-architect
			\begin{description} \itemsep0pt \parskip0pt \parsep0pt
				\item[/lib] - team-leader (on developer's demand he requests a library addition to the upper \textit{lib} directory and updates the dependency dictionary \& scripts)
				\item[/src] - team-leader, devs, designers
				\item[/test] - team-leader, testers
				\item[/report] - team-leader, devs.
				\item[/profile] - testers
				\item[/doc] - senior-architect, team-leader, devs
				\item[/resource] - designers
				\item[/script] - all members
				\item[Note:] - /resource directory and design employees do not exist on 'core-api' component
			\end{description}
		\end{description}
		\item[/test] - Q\&A team, lead-architect
		\item[/doc] - CPO, CTO, lead-architect
		\item[/script] - DevOp, Q\&A teams
		\item[/build] - CPO, DevOp team
	\end{description}
\end{description}

\subsubsection*{Frameworks, Platforms \& Other Third-Parties}

\textbf{Development:}
\begin{itemize}
	\item \textbf{IDEs}: Eclipse (for Java), PyCharm (for Python)
	\item \textbf{TextEditors:} Sublime Text 2, Vim
	\item \textbf{Frameworks:} GWT (google web toolkit), Android SDK, Mahout (Map-Reduce for Machine Learning), Java Spring
	\item \textbf{Development Platforms:} Amazon EC2 (Amazon Elastic Cloud Compute), J2EE (Java2 Platform Enterprise Edition), MongoDB (schemaless database), RabitMQ (message queues), ElasticSearch (text search engine), Puppet (service deployment \& system administration), Hadoop Map-Reduce (batch data processing), Storm (realtime data processing), Pentaho BI (business intelligence platform -- reporting, dashboards, etc)
	\item \textbf{Compilers \& interpreters:} javac (from JRE), python, bash (unix default), tex compiler
	\item \textbf{Other Third-Parties:} ec2-api-tools, amazon-s3-tools (cloud storage), apache-ant libraries (build targets), XML/CSV/JSON toolkits (read, write, parse, etc)
\end{itemize}

\textbf{Software Configuration and Management tools:}
\begin{itemize}
	\item \textbf{Code Versioning:} Atlassian Bitbucket \footnote{www.atlassian.com/software/bitbucket/overview}
	\item \textbf{Team Collaboration:} Atlassian Confluence \footnote{www.atlassian.com/software/confluence/overview/team-collaboration-software}, GTalk, GCalendar, GMail
	\item \textbf{Build System:} Atlassian Bamboo \footnote{www.atlassian.com/software/bamboo/overview}
	\item \textbf{Issue Tracking:} Atlassian Jira \footnote{www.atlassian.com/software/jira/overview} and Atlassian Bonfire \footnote{www.atlassian.com/software/bonfire/overview}
	\item \textbf{Code Review \& Quality:} Atlassian Crucible \footnote{www.atlassian.com/software/crucible/overview}
	\item \textbf{Project History Browsing:} Atlassian FishEye \footnote{www.atlassian.com/software/fisheye/overview}
	\item \textbf{Note:} we have chosen Atlassian cloud-based on-demand services \footnote{www.atlassian.com/software/ondemand/overview/agile-teams} for its ease use \& integration within the stack of tools. Jira - Bitbucket - Bamboo - Crucible are automatically connected with each other; for instance we can refer to Jira which commit has solved an issue by using smart-commit \footnote{confluence.atlassian.com/display/FISHEYE/Using+smart+commits} commands.
\end{itemize}

\pagebreak

\section{Naming}

\begin{center}
\begin{table}[htb]
  \centering
  \caption{Naming convention rules for configuration items.}
  \begin{tabular}{|l|*{2}{l|}}
    \hline
    \textbf{Configuration Item} & \textbf{Naming Convention Rule} & \textbf{Example} \\
    \hline
    lib:internal & [lib-name]-[version] & core-api-1.2.3.jar \\
    \hline
    lib:external & \textit{external rules} & sl4j-1.7.2.jar \\
	\hline
    lib:dictionary & [comp-name].dict & mob-app.dict \\
    \hline
	source-code & [java-class-name].java & StartUI.java \\
	\hline
	test &  [java-test-case-name]Tests.java & SearchWrapperImplTests.java  \\
	\hline
	report & [comp-name]-str[ddMMyyyy].tex & core-api-str01032013.tex \\
	\hline
	profile & [comp-name]-[ddMMyyyy].hprof & core-api-04052012.hprof \\
	\hline
	comp-doc & [comp-name]-[type-abrev].tex & webapp-sds.tex\\
	\hline
	prod-doc & [prod-name]-[type-abrev].tex & caretracker-srs.tex\\
	\hline
	resource & [resource-name].png|wav|... & login-icon-small.png \\	
	\hline
	script & [script-name].[sh,py] & clean-log.sh \\
	\hline
	build & \textit{see chapter~\ref{ch:building}} & - \\
  \hline
  \end{tabular}
  \label{table:reports}
\end{table}
	srs\footnote{software requirements specifications}, str\footnote{software test report}, sds\footnote{software design specifications}
\end{center}

