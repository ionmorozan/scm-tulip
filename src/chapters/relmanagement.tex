\chapter{Release Management}
\label{ch:relmanage}

Release management is the process of releasing and distributing our product items to the customers. The application that is published outside the development environment is called a product release. A product release is essentially a stable build for the production environment, a system build that was fully debugged, tested, reviewed, verified, validated and audited. The release management also includes the deployment process as well as the update of related metadata that goes into tracking a given version of our software application. To continue, we are going to address and answer the following questions:

\begin{itemize}
	\item \textit{Who is our customer?} \\
	Our company has internal and external customers. The internal customers we could say that are the front-end teams. They count on the CoreAPI interfaces developed by the backend team. So, in terms of an internal release the deliverable is just a package with only the CoreAPI's interfaces. Our external customers, as we explained in chapter~\ref{ch:intro}, are divided into three types: clinical institutions, private physicians/practitioners and patients. For each one of them we offer a different login session with access to specific functionalities. In terms of marketing these access rules/sessions are described as three product variants: CareTracker\textsuperscript{\textregistered} ProClinical, ProPhy and CompletePatient. The last product variant is also available on mobile platforms (through GooglePlay Android app-market). Moreover, the CoreAPI itself is externalized on request for open-data driven research institutions or communities.  
	\item \textit{What types of releases do we offer?} \\
	We generally aim to offer an official full release per year. But, it might happen in some circumstances (severe bugs or high priority future requests) to deploy a temporary release, i.e. a release after the second iteration. In that case the release will have a distinct name and version compared with the official one.
	\item \textit{When and why a package is released? Who is responsible for that?} \\
	We release a package once every year, at the end of the third iteration. This responsibility goes to the product's owner, our CPO, and the DevOps teams. We have 3 iterations per year, 4 months each. Usually, in the last iteration few developments are made, but on the other hand we thoroughly check our product for any issues. In addition, we consider that in one year our product reaches a good level of maturity and stability to be deployed without any concerns into production environment. So, as you can see the release management process is tightly bound to our \textit{iterative} model of SCM life cycle.
	\item \textit{What exactly is included in each release?} \\
	From an inside perspective a product release includes the latest stable components' builds and product documents (user manuals, product specifications etc), see chapter~\ref{ch:baselining} - Release Baseline. From the user's perspective a release is simply a deployment of some new updates: new features and/or bug-fixes, better tutorials, revised manuals and more lessons on the e-learning platform. The release process is transparent to them, given that the updates are deployed automatically without any manual intervention. Thus, a user always has the newest version of our product. Of course, we are not applying any updates before our user's acceptance. 
	\item \textit{How a release is distributed?} \\ 
	Short to say, we replace the current release by deploying on Amazon EC2 the new one, without any down-times. Then, the new release is published into repository's active section and the older one goes to the archive section. For our future users the newest product is available to purchase on Tulip's online market repository (web-app) or GooglePlay Android app-market (mobile-app). This process is similar with the way that, for instance, Google deploys new releases for its online services (GMail, Youtube, etc) and corresponding mobile applications.
\end{itemize} 

\section{Maintenance}

The possibility exists that our just released and well tested product contains some bugs. If that is the case, we immediately fix them in the ongoing iteration and we schedule a fast-release at the end of it. More interesting is how we track the bugs and where do we merge all the fixes. Given that a release has a version number associated in time with a specific stable build and that every feature has separate development and bug-fix branches, we can easily track and solve the source of the bug. Then, during a present iteration the responsible team creates a new branch for the feature's bug-fix (conform our naming convention rules), solves the issues, and merges all the changes into the their own team-branch if the unit and regression test-suites were successful. Now, a new bug for the same feature will point/track to the last fix! 

\section{Naming}

We have established a naming convention that mainly indicates the type of release (official, bug-fix, prior-feature) and its launching period (0 = after first iteration, 2 = after second, 4 = after the last one). For example, release \textbf{caretracker-2.0.ft} is the release that includes all the components' build (at least one with an extra feature added) and other CIs from the release-baseline established at the end of the first iteration, i.e \textit{mob-app-2.b1.2, web-app-2.b1.1, core-api-2.b1.4} builds and the product documents, \textit{prod-doc}, tagged for release. More detailed aspects about naming are found in table~\ref{table:release}.

\pagebreak

\begin{center}
\begin{table}[htb]
  \centering
  \caption{Naming convention rules for product releases} 
  \begin{tabular}{|p{1.15cm}|p{1.8cm}|p{2.9cm}|p{3.7cm}|p{2.8cm}|}
  	\hline
    \textbf{Release Type} & \textbf{Naming Convention Rule} & \textbf{Includes} & \textbf{Description} & \textbf{Example} \\
    \hline
    Official & [product]-[version] & All CIs from the third's iteration release-baseline & Is the official release and always contains the product in its last planed and stable form & caretracker-1.4 \\
  	\hline
  	Bug-fix (bf) & [product]-[version].bf & All CIs from the intermediate iteration (first/second) release-baseline & A fast-release where one or more components contain(s) only bug-fixes and others very few implemented features. Also, there might be cases where every component has its own fixes and extra features. & caretracker-1.0.bf, caretracker-1.2.bf \\
  	\hline
  	Prior-feature (ft) & [product]-[version].ft & All CIs from the intermediate iteration (first/second) release-baseline & A fast-release where at least one or more components contain(s) important features and few bug-fixes (if exist). & caretracker-1.0.ft, caretracker-1.2.ft \\
  	\hline
  \end{tabular}
  \label{table:release}
\end{table}
\end{center}

For the version number, we have decided to use two levels of numbers connected with periods (e.g. 2.2). The first number is associated with \textbf{Major} version, (incremented at each annual release). The second number is the \textbf{Minor} version, which points to the iteration period -- 0 for the first, 2 for the second and 4 for the last iteration (the official release).



