Requests for changes are frequent and change management is a pervasive subject through the entire software 
development process. Keeping this in mind and the fact that change management always tends to get more 
complex in time, we use Atlassian Jira for Issue Tracking and Atlassian FishEye for viewing the project's
history.

\section{Initiation}

The major sources of change are :
\begin{itemize}
 \item New software requirements
 \item Bug fixes found during testing
 \item Correcting unanticipated consequences
 \item Responding to customer complaints 
\end{itemize}

\vspace*{0.2cm}

The first category mostly includes the changes which come from the customers' feedback. These are well
planned changes and are relatively easy to integrate in the product, in a reasonable time frame. These
changes are also well spaced in time, customer feedback being gathered with every new release.

The second category is represented by minor changes (affecting one software module). These are not well
spaced in time, but they can be easily implemented.

The third category is usually comprised of major changes. Such situations are very problematic because they 
can lead to the need of changing the customer's agreement with the developer (they can affect 
the terms and conditions mentioned in the contract). Resolution of the underlying problems comes with additional
costs for the customer and may require to push back deadlines.
 
The fourth category is about unplanned feedback (from customers) which needs to be given high priority.
The underlying problems for these types off changes usually require resolution in the current iteration.

The general change management procedure is described in \labelindexref{Figure}{img:general-change-flow}.
This change control flow applies to most types of changes, during software development. The process
starts with the \emph{change originator} which is aware of the current state of the project or of the complaints /
requirement expressed by the customers and argues for a certain change. The change originator can be just about 
anyone: a developers, the product analyst, one of the software architects, a team leader. A Change Request (CR) is
created to accurately describe the wanted change. The Change Request is based on a Change Request Form (in the case
of new feature) or Problem Report Form (when an issue needs to be resolved). The CR will have assigned to it an 
initial priority. If it is considered to be an emergency it will immediately be classified as approved so work can 
begin, but the process described here, of analysis and evaluation, will still continue in order to reach a final decision.
The CR is analyzed to establish what costs does it imply and its overall impact on the project. The next phase is evaluation,
where the Change Control Board decides if the change is accepted for implementation, postponed or rejected. If it is rejected,
the change originator will be notified of the decision and of the reasons for it. If it is accepted, the next pahse is implementation. Here, the items that need 
to be changed are checked out from the configuration library. They are modified according to the CR and the entire suite of tests 
is executed on them (from unit testing to integration testing). After testing, validation and verification is complete the new 
versions of the configuration items are placed under SCM control.

An emergency change will not be placed under SCM control until it is approved by the Change Control Board. If an emergency change is reject all artifacts 
created in an effort to resolve the emergency will be discarded.



One important exception to this flowchart is represented by the third category of changes: 
``Correcting unanticipated consequences''. The Change Control Board cannot decide alone to make such modifications and needs
approval from the client. A more appropriate diagram for this case is shown in \labelindexref{Figure}{img:major-change-flow}



\fig[scale=0.45]{images/general-change-flow.pdf}{img:general-change-flow}{Change management procedure}

\fig[scale=0.45]{images/major-change-flow.pdf}{img:major-change-flow}{Major change procedure}

\newpage


\section{Analysis}

The main purpose of \emph{change analysis} is to identify the potential consequences of a change. Change analysis focuses on \emph{traceability} and \emph{dependency}. In traceability, we try to capture the relations between requirements, specifications and design elements. In dependency, we focus 
on the relations between logical components. Change analysis estimates the costs in resources and time to make a certain change to the system and will make 
recommendations about the cost-effectiveness of such a change. Different people are involved in the analysis phase, depending on the type of change:  
\begin{itemize}
 \item For changes which imply new software requirements, it is done by a Quality Assurance engineer, the Product Analyst and the Senior Architect, 
      for front-end or the back-end (depending on what part of the system the new requirement refers to).
 \item For bug fixes, it is done by a Quality Assurance engineer, the team leader and 3-5 software engineers.
 \item For correcting unanticipated consequences, it is done by a Quality Assurance engineer and all three architects.
 \item For responding to customer complaints, it is done by a Quality Assurance engineer, the Product Analyst and a Senior Architect.
\end{itemize}


  
